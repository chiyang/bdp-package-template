import sys
if len(sys.argv) >= 2:
    name = sys.argv[1]
else:
    name = 'noname'

sys.stdout.write('Hello, ' + name + '! (standard output)\n')
sys.stderr.write('Hello, ' + name + '! (standard error)\n')