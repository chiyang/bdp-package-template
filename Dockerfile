# Base Image
FROM biocontainers/biocontainers:latest

# Install packages that needs root privileges
USER root
# Install packages that needs root privileges
# You have install nodejs that we can use docker-wrapper to wrap your scripts
ENV NODE_PATH=/usr/lib/node_modules
RUN apt-get update && \
    curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
    apt-get install -y nodejs && \
    apt-get clean && \
    apt-get purge --auto-remove -y curl

USER biodocker
# Install library/packages that do not require root privileges
# Installations of bdp-docker-wrapper and bdp-task-adapter are required.
# The channels r and bioconda are available by default.

# Copy the `docker-wrapper.js` and `bdp-tasks.yaml` into the home directory.
COPY --chown=biodocker:biodocker ["./docker-wrapper.js", "./bdp-tasks.yaml", "/home/biodocker/"]
COPY --chown=biodocker:biodocker ["./bdp-package", "/home/biodocker/bdp-package"]
# Copy your scripts
COPY --chown=biodocker:biodocker ["./my-scripts", "/home/biodocker/scripts/"]
RUN npm install bdp-docker-wrapper@0.4.7 --prefix /home/biodocker && \
    npm install bdp-task-adapter@0.9.7-alpha.5 --prefix /home/biodocker/bdp-package/scripts
# Switch back to root.
# Don't worry, docker-wrapper changes the user id/group id to the external user's uid/gid to ensure that you do not write files of root privileges.
USER root
ENTRYPOINT ["node", "/home/biodocker/docker-wrapper.js"]
